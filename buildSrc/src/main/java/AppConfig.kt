object AppConfig {
    const val compileSdkVersion = 33
    const val namespace = "tia.sarwoedhi.newsapp"
    const val minSdkVersion = 24
    const val targetSdkVersion = 33
    const val versionCode = 1
    const val versionName= "1"
    const val testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

}