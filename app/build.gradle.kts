plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("kapt")
    id("com.google.dagger.hilt.android")
    id("androidx.navigation.safeargs")
}

android {
    namespace = AppConfig.namespace
    compileSdk = AppConfig.compileSdkVersion

    defaultConfig {
        applicationId = AppConfig.namespace
        minSdk = AppConfig.minSdkVersion
        targetSdk = AppConfig.targetSdkVersion
        versionCode = AppConfig.versionCode
        versionName = AppConfig.versionName
        testInstrumentationRunner = AppConfig.testInstrumentationRunner
    }

    buildFeatures {
        viewBinding = true
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = true
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }

        getByName("debug") {
            applicationIdSuffix = ".debug"
            isDebuggable = true
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }

    kotlin {
        jvmToolchain {
            this.languageVersion.set(JavaLanguageVersion.of(11))
        }
    }


    kotlinOptions {
        jvmTarget = "11"
    }

}

dependencies {

    //androidX
    implementation(Dependencies.AndroidXSupport.implementations)

    //materialDesign
    implementation(Dependencies.MaterialDesign.google)
    implementation(Dependencies.MaterialDesign.glide)

    //test
    testImplementation(Dependencies.JUnit.testImplementation)
    androidTestImplementation(Dependencies.Test.androidTestImplementation)

    //network
    implementation(Dependencies.Network.implementations)

    // Hilt
    kapt(Dependencies.Hilt.kapt)
    implementation(Dependencies.Hilt.implementation)

    //coroutine
    implementation(Dependencies.Coroutine.implementation)
}