package tia.sarwoedhi.newsapp.core.data.remote.repository.article.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import tia.sarwoedhi.newsapp.core.data.remote.model.response.article.ArticleItem
import tia.sarwoedhi.newsapp.core.data.remote.repository.article.source.ArticleRemoteDataSource
import tia.sarwoedhi.newsapp.core.data.utils.ApiResult
import tia.sarwoedhi.newsapp.core.data.utils.Const.PAGE_SIZE

class ArticlePagingSource(
    private val articleRemoteDataSource: ArticleRemoteDataSource,
    private val sources: String,
    private val query:String,
    private val lang:String
) : PagingSource<Int, ArticleItem>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ArticleItem> {

        val nextPageNumber = params.key ?: 1

        return when (val response =
            articleRemoteDataSource.getArticleBySources(
                sources = sources,
                page = nextPageNumber,
                q = query,
                language = lang,
                pageSize = PAGE_SIZE
            )) {
            is ApiResult.Success -> {
                val repos = response.data?.list ?: listOf()
                val nextKey = if (repos.isEmpty()) {
                    null
                } else {
                    nextPageNumber + (params.loadSize / 6)
                }
                LoadResult.Page(
                    data = repos,
                    prevKey = null,
                    nextKey = nextKey
                )
            }

            is ApiResult.Error -> {
                LoadResult.Error(Throwable(message = response.error))
            }
        }

    }

    override fun getRefreshKey(state: PagingState<Int, ArticleItem>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }
}