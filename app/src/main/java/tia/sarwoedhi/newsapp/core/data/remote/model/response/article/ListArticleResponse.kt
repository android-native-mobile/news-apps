package tia.sarwoedhi.newsapp.core.data.remote.model.response.article


import com.google.gson.annotations.SerializedName

data class ListArticleResponse(
    @field:SerializedName("articles")
    val list: List<ArticleItem>,
    @field:SerializedName("status")
    val status: String,
)