package tia.sarwoedhi.newsapp.core.data.remote.repository.category.source

import tia.sarwoedhi.newsapp.core.data.remote.model.response.category.ListSourceResponse
import tia.sarwoedhi.newsapp.core.data.utils.ApiResult

interface CategoryRemoteDataSource {

    suspend fun getListCategory(): ApiResult<ListSourceResponse>

    suspend fun getListSourcesByCategory(category: String, name:String): ApiResult<ListSourceResponse>

}
