package tia.sarwoedhi.newsapp.core.data.remote.model.response.category

import com.google.gson.annotations.SerializedName

data class ListSourceResponse(

    @field:SerializedName("sources")
    val articles: List<Source>,

    @field:SerializedName("status")
    val status: String
)