package tia.sarwoedhi.newsapp.core.data.remote.repository.article

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import tia.sarwoedhi.newsapp.core.data.remote.repository.article.paging.ArticlePagingSource
import tia.sarwoedhi.newsapp.core.data.remote.repository.article.source.ArticleRemoteDataSource
import tia.sarwoedhi.newsapp.core.data.utils.Const.PAGE_SIZE
import tia.sarwoedhi.newsapp.domain.entities.ArticleEntity
import tia.sarwoedhi.newsapp.domain.mapper.toArticleEntity
import tia.sarwoedhi.newsapp.domain.repository.article.ArticleRepository
import javax.inject.Inject

class ArticleRepositoryImpl @Inject constructor(private val dataSource: ArticleRemoteDataSource) :
    ArticleRepository {
    override fun getListArticle(sources: String, language : String,q: String): Flow<PagingData<ArticleEntity>> {
        return Pager(PagingConfig(PAGE_SIZE, 5)) {
            ArticlePagingSource(dataSource, sources, lang = language, query = q)
        }.flow.map { pagingData ->
            pagingData.map {
                it.toArticleEntity()
            }
        }
    }


}
