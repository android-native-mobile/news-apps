package tia.sarwoedhi.newsapp.core.di.binds

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import tia.sarwoedhi.newsapp.core.data.remote.repository.category.CategoryRepositoryImpl
import tia.sarwoedhi.newsapp.core.data.remote.repository.category.source.CategoryRemoteDataSource
import tia.sarwoedhi.newsapp.core.data.remote.repository.category.source.CategoryRemoteDataSourceImpl
import tia.sarwoedhi.newsapp.domain.repository.category.CategoryRepository

@InstallIn(ViewModelComponent::class)
@Module
abstract class CategoryBindModule {

    @Binds
    @ViewModelScoped
    abstract fun bindCategoryRemoteDs(impl: CategoryRemoteDataSourceImpl): CategoryRemoteDataSource

    @Binds
    @ViewModelScoped
    abstract fun bindCategoryRepository(impl: CategoryRepositoryImpl): CategoryRepository

}