package tia.sarwoedhi.newsapp.core.data.remote.repository.article.source

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import tia.sarwoedhi.newsapp.core.data.remote.api.ArticleApi
import tia.sarwoedhi.newsapp.core.data.remote.base.BaseDataSourceImpl
import tia.sarwoedhi.newsapp.core.data.remote.model.response.article.ListArticleResponse
import tia.sarwoedhi.newsapp.core.data.utils.ApiResult
import tia.sarwoedhi.newsapp.core.di.dispatcher.IoDispatcher
import javax.inject.Inject

class ArticleRemoteDataSourceImpl @Inject constructor(
    private val articleApi: ArticleApi,
    @IoDispatcher private val dispatcher: CoroutineDispatcher
) : BaseDataSourceImpl(), ArticleRemoteDataSource {

    override suspend fun getArticleBySources(sources: String,language:String, q:String, page: Int, pageSize: Int): ApiResult<ListArticleResponse> {
        return executeWithResponse {
            withContext(dispatcher) {
                articleApi.getListArticleByCategory(
                    sources = sources,
                    language= language,
                    query = q,
                    page = page,
                    pageSize = pageSize
                )
            }
        }
    }


}
