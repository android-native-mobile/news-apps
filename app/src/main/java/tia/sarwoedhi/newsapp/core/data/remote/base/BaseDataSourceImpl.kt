package tia.sarwoedhi.newsapp.core.data.remote.base


import retrofit2.Response
import tia.sarwoedhi.newsapp.core.data.utils.ApiResult
import java.lang.Exception

abstract class BaseDataSourceImpl {

    protected inline fun <reified T> executeWithResponse(block: () -> Response<T>): ApiResult<T & Any> {
        try {
            val response = block()
            if (!response.isSuccessful) {
                 return ApiResult.Error(response.message(), response.errorBody())
            }
            val body = response.body()
            return ApiResult.Success(body)
        } catch (e:Exception) {
            return ApiResult.Error(e.message ?: "", Exception())
        }
    }

}