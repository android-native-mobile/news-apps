package tia.sarwoedhi.newsapp.core.data.remote.repository.category.source

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import tia.sarwoedhi.newsapp.core.data.remote.api.CategoryApi
import tia.sarwoedhi.newsapp.core.data.remote.base.BaseDataSourceImpl
import tia.sarwoedhi.newsapp.core.data.remote.model.response.category.ListSourceResponse
import tia.sarwoedhi.newsapp.core.data.utils.ApiResult
import tia.sarwoedhi.newsapp.core.di.dispatcher.IoDispatcher
import javax.inject.Inject

class CategoryRemoteDataSourceImpl @Inject constructor(
    private val categoryApi: CategoryApi,
    @IoDispatcher private val dispatcher: CoroutineDispatcher
) : BaseDataSourceImpl(), CategoryRemoteDataSource {

    override suspend fun getListCategory(): ApiResult<ListSourceResponse> {
        return executeWithResponse {
            withContext(dispatcher) {
                categoryApi.getListCategory()
            }
        }
    }

    override suspend fun getListSourcesByCategory(category: String, name:String): ApiResult<ListSourceResponse> {
        return executeWithResponse {
            withContext(dispatcher) {
                categoryApi.getListSourcesByCategory(category, name)
            }
        }
    }

}
