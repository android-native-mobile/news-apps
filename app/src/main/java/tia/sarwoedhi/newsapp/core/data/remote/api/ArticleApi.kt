package tia.sarwoedhi.newsapp.core.data.remote.api

import retrofit2.Response
import retrofit2.http.*
import tia.sarwoedhi.newsapp.core.data.remote.model.response.article.ListArticleResponse

interface ArticleApi {

    @GET("everything")
    suspend fun getListArticleByCategory(
        @Query("sources") sources: String,
        @Query("page") page: Int,
        @Query("language") language: String,
        @Query("qIntitle") query: String,
        @Query("pageSize") pageSize: Int
    ): Response<ListArticleResponse>

}