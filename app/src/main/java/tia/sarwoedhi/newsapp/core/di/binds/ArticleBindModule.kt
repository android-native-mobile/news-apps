package tia.sarwoedhi.newsapp.core.di.binds

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import tia.sarwoedhi.newsapp.core.data.remote.repository.article.ArticleRepositoryImpl
import tia.sarwoedhi.newsapp.core.data.remote.repository.article.source.ArticleRemoteDataSource
import tia.sarwoedhi.newsapp.core.data.remote.repository.article.source.ArticleRemoteDataSourceImpl
import tia.sarwoedhi.newsapp.domain.repository.article.ArticleRepository

@InstallIn(ViewModelComponent::class)
@Module
abstract class ArticleBindModule {

    @Binds
    @ViewModelScoped
    abstract fun bindArticleRemoteDataSource(impl: ArticleRemoteDataSourceImpl): ArticleRemoteDataSource

    @Binds
    @ViewModelScoped
    abstract fun bindAuthRepository(impl: ArticleRepositoryImpl): ArticleRepository

}