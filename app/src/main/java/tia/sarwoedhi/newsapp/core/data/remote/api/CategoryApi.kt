package tia.sarwoedhi.newsapp.core.data.remote.api

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import tia.sarwoedhi.newsapp.core.data.remote.model.response.category.ListSourceResponse

interface CategoryApi {

    @GET("top-headlines/sources")
    suspend fun getListCategory(): Response<ListSourceResponse>

    @GET("top-headlines/sources")
    suspend fun getListSourcesByCategory(
        @Query("category") category: String,
        @Query("language") name: String
    ): Response<ListSourceResponse>

}