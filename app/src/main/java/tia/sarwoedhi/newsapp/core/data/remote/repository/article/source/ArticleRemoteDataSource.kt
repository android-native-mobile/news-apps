package tia.sarwoedhi.newsapp.core.data.remote.repository.article.source

import tia.sarwoedhi.newsapp.core.data.remote.model.response.article.ListArticleResponse
import tia.sarwoedhi.newsapp.core.data.utils.ApiResult

interface ArticleRemoteDataSource {


    suspend fun getArticleBySources(
        sources: String,
        language: String,
        q: String,
        page: Int,
        pageSize: Int
    ): ApiResult<ListArticleResponse>

}
