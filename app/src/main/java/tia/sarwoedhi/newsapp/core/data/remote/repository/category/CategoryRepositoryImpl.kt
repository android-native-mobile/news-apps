package tia.sarwoedhi.newsapp.core.data.remote.repository.category

import tia.sarwoedhi.newsapp.core.data.remote.repository.category.source.CategoryRemoteDataSource
import tia.sarwoedhi.newsapp.core.data.utils.ApiResult
import tia.sarwoedhi.newsapp.domain.entities.CategoryEntity
import tia.sarwoedhi.newsapp.domain.entities.SourcesEntity
import tia.sarwoedhi.newsapp.domain.mapper.toCategoryEntity
import tia.sarwoedhi.newsapp.domain.mapper.toSourcesEntity
import tia.sarwoedhi.newsapp.domain.repository.category.CategoryRepository
import tia.sarwoedhi.newsapp.domain.utils.DomainWrapper
import javax.inject.Inject

class CategoryRepositoryImpl @Inject constructor(private val dataSource: CategoryRemoteDataSource) :
    CategoryRepository {
    override suspend fun getCategoryList(): DomainWrapper<List<CategoryEntity>> {
        return when (val result = dataSource.getListCategory()) {

            is ApiResult.Success -> {
                DomainWrapper.Success(result.data?.articles?.map { it.toCategoryEntity() }?.distinctBy {  it.category } ?: listOf())
            }

            is ApiResult.Error -> {
                DomainWrapper.Error(result.error)
            }

        }
    }


    override suspend fun getSourcesList(category: String, name:String): DomainWrapper<List<SourcesEntity>> {
      return when (val result = dataSource.getListSourcesByCategory(category, name)) {
            is ApiResult.Success -> {
                DomainWrapper.Success(result.data?.articles?.map { it.toSourcesEntity() }?.distinct() ?: listOf())
            }

            is ApiResult.Error -> {
                DomainWrapper.Error(result.error)
            }
        }
    }
}