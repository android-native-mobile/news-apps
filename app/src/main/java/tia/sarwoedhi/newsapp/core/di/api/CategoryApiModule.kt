package tia.sarwoedhi.newsapp.core.di.api

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import tia.sarwoedhi.newsapp.core.data.remote.api.CategoryApi
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object CategoryApiModule {

    @Provides
    @Singleton
    fun provideCategoryApi(retrofit: Retrofit): CategoryApi {
        return retrofit.create(CategoryApi::class.java)
    }

}