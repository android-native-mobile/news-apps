package tia.sarwoedhi.newsapp.core.data.remote.network

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class HttpInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        return chain.proceed(addHeader(chain.request()))
    }

    //
    //af0320766b9a406eb4bf545ca055813c
    //if failed please change the apiKey
    private fun addHeader(oriRequest: Request): Request {
        return oriRequest.newBuilder()
            .addHeader("Content-Type", "application/json")
            .addHeader("Authorization", "6ec55d370dea4c0b91467054088784da")
            .build()
    }
}