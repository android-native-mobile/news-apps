package tia.sarwoedhi.newsapp.core.di.api

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import tia.sarwoedhi.newsapp.core.data.remote.api.ArticleApi
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ArticleApiModule {

    @Provides
    @Singleton
    fun provideArticleApi(retrofit: Retrofit): ArticleApi {
        return retrofit.create(ArticleApi::class.java)
    }

}