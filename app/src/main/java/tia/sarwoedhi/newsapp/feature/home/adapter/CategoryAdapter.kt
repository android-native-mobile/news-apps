package tia.sarwoedhi.newsapp.feature.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import tia.sarwoedhi.newsapp.R
import tia.sarwoedhi.newsapp.databinding.ItemCategoryBinding
import tia.sarwoedhi.newsapp.domain.entities.CategoryEntity

class CategoryAdapter : RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {

    private lateinit var categoryItemClickCallback: OnCategoryItemClickCallback

    private val categoryList: MutableList<CategoryEntity> = mutableListOf()

    fun setOnCategoryItemClickCallback(onCategoryItemClickCallback: OnCategoryItemClickCallback) {
        this.categoryItemClickCallback = onCategoryItemClickCallback
    }

    fun clearCategoryData() {
        categoryList.clear()
        notifyDataSetChanged()
    }


    fun setCategoryData(items: List<CategoryEntity>) {
        categoryList.clear()
        categoryList.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = ItemCategoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = categoryList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindArticle(holder.itemView.context, categoryList[position])
        holder.itemView.setOnClickListener {
            categoryItemClickCallback.onItemClicked(categoryList[position])
        }
    }

    inner class ViewHolder(private val binding: ItemCategoryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindArticle(context: Context, categoryEntity: CategoryEntity) {
            with(binding) {
                if (categoryEntity.category.isNotBlank()) {
                    this.txtTitle.text = String.format(
                        context.resources.getString(R.string.list_contents),
                        categoryEntity.category
                    )
                }

            }
        }
    }
}

interface OnCategoryItemClickCallback {
    fun onItemClicked(data: CategoryEntity)
}