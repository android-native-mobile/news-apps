package tia.sarwoedhi.newsapp.feature.sources

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import tia.sarwoedhi.newsapp.databinding.FragmentSourcesBinding
import tia.sarwoedhi.newsapp.domain.entities.SourcesEntity
import tia.sarwoedhi.newsapp.utils.UiState

@AndroidEntryPoint
class SourcesFragment : Fragment() {

    private lateinit var _binding: FragmentSourcesBinding
    private val binding get() = _binding
    private val viewModel: SourceViewModel by viewModels()
    private lateinit var sourcesAdapter: SourcesAdapter
    private var category = ""
    private var language = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSourcesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        setupSearchView()
    }

    private fun setupView() {
        category = SourcesFragmentArgs.fromBundle(arguments as Bundle).category
        sourcesAdapter = SourcesAdapter()
        getData("")
        sourcesAdapter.setOnSourcesItemClickCallback(object : OnSourcesItemClickCallback {
            override fun onItemClicked(data: SourcesEntity) {
                nextScreen(data)
            }
        })

        with(binding.rvSources) {
            layoutManager = GridLayoutManager(requireContext(), 2)
            adapter = sourcesAdapter
            isNestedScrollingEnabled = true
        }
    }

    private fun getData(name:String){
        viewModel.getListSources(category, name)
        viewModel.sources.flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
            .onEach { resource ->
                sourcesResponse(resource)
            }.launchIn(lifecycleScope)
    }

    private fun nextScreen(data: SourcesEntity) {
        val toArticleFragment = SourcesFragmentDirections.actionSourcesFragmentToArticleFragment()
        toArticleFragment.source = data.id
        toArticleFragment.language = language
        view?.findNavController()?.navigate(toArticleFragment)
    }

    private fun setupSearchView() {
        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            android.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                getData(p0 ?: "")
                language = p0 ?: ""
                return true
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                if(p0?.isEmpty() == true || p0 == null) {
                    getData("")
                    language = ""
                }
                return true
            }
        })
    }


    private fun sourcesResponse(resource: UiState<List<SourcesEntity>>?) {
        when (resource) {
            is UiState.Success -> {
                showLoading(false)
                sourcesAdapter.clearData()
                resource.data?.let {
                    sourcesAdapter.setData(it)
                }

            }

            is UiState.Error -> {
                showLoading(false)
                Toast.makeText(requireContext(), resource.error, Toast.LENGTH_LONG).show()
            }

            is UiState.Loading -> {
                showLoading(true)
            }

            else -> {}
        }
    }

    private fun showLoading(isLoading: Boolean) {
        binding.progressBar.alpha = if (isLoading) 1.0f else 0.0f
        binding.tvMessage.alpha = if (isLoading) 0.0f else 1.0f
    }

}