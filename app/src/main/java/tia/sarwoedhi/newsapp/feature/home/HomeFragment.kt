package tia.sarwoedhi.newsapp.feature.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import tia.sarwoedhi.newsapp.databinding.FragmentHomeBinding
import tia.sarwoedhi.newsapp.domain.entities.CategoryEntity
import tia.sarwoedhi.newsapp.feature.home.adapter.CategoryAdapter
import tia.sarwoedhi.newsapp.feature.home.adapter.OnCategoryItemClickCallback
import tia.sarwoedhi.newsapp.utils.UiState


@AndroidEntryPoint
class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private val homeViewModel: HomeViewModel by viewModels()
    private lateinit var categoryAdapter: CategoryAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setupView() {

        categoryAdapter = CategoryAdapter()

        homeViewModel.getListCategory()
        homeViewModel.categories.flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
            .onEach { resource ->
                categoriesResponse(resource)
            }.launchIn(lifecycleScope)


        categoryAdapter.setOnCategoryItemClickCallback(object : OnCategoryItemClickCallback {
            override fun onItemClicked(data: CategoryEntity) {
                nextScreen(data)
            }
        })

        with(binding.rvCategory) {
            layoutManager = GridLayoutManager(requireContext(), 2)
            adapter = categoryAdapter
            isNestedScrollingEnabled = true
        }
    }

    private fun categoriesResponse(resource: UiState<List<CategoryEntity>>?) {
        when (resource) {
            is UiState.Success -> {
                showLoading(false)
                categoryAdapter.clearCategoryData()
                resource.data?.let {
                    categoryAdapter.setCategoryData(it)
                }
            }

            is UiState.Error -> {
                showLoading(false)
                Toast.makeText(requireContext(), resource.error, Toast.LENGTH_LONG).show()
            }

            is UiState.Loading -> {
                showLoading(true)
            }

            else -> {}
        }
    }

    private fun nextScreen(data: CategoryEntity) {
        val toSourcesFragment = HomeFragmentDirections.actionHomeFragmentToSourcesFragment()
        toSourcesFragment.category = data.category
        view?.findNavController()?.navigate(toSourcesFragment)
    }

    private fun showLoading(isLoading: Boolean) {
        binding.progressBar.alpha = if (isLoading) 1.0f else 0.0f
        binding.tvMessage.alpha = if (isLoading) 0.0f else 1.0f
    }

}