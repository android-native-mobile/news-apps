package tia.sarwoedhi.newsapp.feature.home

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import tia.sarwoedhi.newsapp.domain.entities.CategoryEntity
import tia.sarwoedhi.newsapp.domain.usecase.CategoryUseCase
import tia.sarwoedhi.newsapp.utils.UiState
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val categoryUseCase: CategoryUseCase) :
    ViewModel() {

    private val _categories: MutableStateFlow<UiState<List<CategoryEntity>>> = MutableStateFlow(UiState.Loading)
    val categories: StateFlow<UiState<List<CategoryEntity>>> get() = _categories

    fun getListCategory() {
        viewModelScope.launch {
            categoryUseCase.invoke().stateIn(
                scope = viewModelScope,
                started = SharingStarted.WhileSubscribed(5_000),
                initialValue = UiState.Loading
            ).collect { result ->

                _categories.value = result
            }
        }
    }

}