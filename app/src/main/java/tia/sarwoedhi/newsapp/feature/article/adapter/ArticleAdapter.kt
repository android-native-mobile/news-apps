package tia.sarwoedhi.newsapp.feature.article.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import tia.sarwoedhi.newsapp.R
import tia.sarwoedhi.newsapp.databinding.ItemArticleBinding
import tia.sarwoedhi.newsapp.domain.entities.ArticleEntity


class ArticleAdapter : PagingDataAdapter<ArticleEntity, ArticleAdapter.ViewHolder>(DIFF_CALLBACK) {

    private lateinit var articleItemClickCallback: OnArticleItemClickCallback


    fun setOnArticleItemClickCallback(onArticleItemClickCallback: OnArticleItemClickCallback) {
        this.articleItemClickCallback = onArticleItemClickCallback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = ItemArticleBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let { entity ->
            holder.bindArticle(holder.itemView.context, entity)
            holder.itemView.setOnClickListener {
                articleItemClickCallback.onItemClicked(entity)
            }
        }


    }

    inner class ViewHolder(private val binding: ItemArticleBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindArticle(context: Context, articleEntity: ArticleEntity) {
            with(binding) {
                if (articleEntity.title.isNotBlank()) {
                    this.txtTitle.text = String.format(
                        context.resources.getString(R.string.list_contents),
                        articleEntity.title
                    )
                }

                if (articleEntity.description.isNotBlank()) {
                    txtOverview.text = String.format(
                        context.resources.getString(R.string.list_contents),
                        articleEntity.description
                    )
                }

                if (articleEntity.urlToImage.isNotBlank()) {
                    lblImgNotAvailable.visibility = View.INVISIBLE
                    Glide.with(context)
                        .load(articleEntity.urlToImage)
                        .into(imgPoster)
                } else {
                    Glide.with(context)
                        .load(R.drawable.shape_img_not_available)
                        .into(imgPoster)
                    lblImgNotAvailable.visibility = View.VISIBLE
                }
            }
        }
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ArticleEntity>() {
            override fun areItemsTheSame(oldItem: ArticleEntity, newItem: ArticleEntity): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(
                oldItem: ArticleEntity,
                newItem: ArticleEntity
            ): Boolean {
                return oldItem.title == newItem.title && oldItem.title.isNotEmpty() && newItem.title.isNotEmpty()
            }
        }
    }
}

interface OnArticleItemClickCallback {
    fun onItemClicked(data: ArticleEntity)
}