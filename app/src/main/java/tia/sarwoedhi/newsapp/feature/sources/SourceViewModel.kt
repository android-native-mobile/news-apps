package tia.sarwoedhi.newsapp.feature.sources

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import tia.sarwoedhi.newsapp.domain.entities.SourcesEntity
import tia.sarwoedhi.newsapp.domain.usecase.SourcesUseCase
import tia.sarwoedhi.newsapp.utils.UiState
import javax.inject.Inject

@HiltViewModel
class SourceViewModel @Inject constructor(private val sourcesUseCase: SourcesUseCase) : ViewModel() {

    private val _sources: MutableStateFlow<UiState<List<SourcesEntity>>> =
        MutableStateFlow(UiState.Loading)
    val sources: StateFlow<UiState<List<SourcesEntity>>> get() = _sources

    fun getListSources(category: String, name:String) {
        viewModelScope.launch {
            sourcesUseCase.invoke(category, name).stateIn(
                scope = viewModelScope,
                started = SharingStarted.WhileSubscribed(5_000),
                initialValue = UiState.Loading
            ).collect { result ->
                _sources.value = result
            }
        }
    }

}