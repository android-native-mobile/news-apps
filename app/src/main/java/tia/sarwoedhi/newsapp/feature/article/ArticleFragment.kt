package tia.sarwoedhi.newsapp.feature.article

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import tia.sarwoedhi.newsapp.databinding.FragmentArticleBinding
import tia.sarwoedhi.newsapp.domain.entities.ArticleEntity
import tia.sarwoedhi.newsapp.feature.article.adapter.ArticleAdapter
import tia.sarwoedhi.newsapp.feature.article.adapter.OnArticleItemClickCallback
import tia.sarwoedhi.newsapp.feature.home.adapter.LoadingStateAdapter

@AndroidEntryPoint
class ArticleFragment : Fragment() {
    private lateinit var _binding: FragmentArticleBinding
    private val binding get() = _binding
    private val articleViewModel: ArticleViewModel by viewModels()
    private lateinit var articleAdapter: ArticleAdapter
    private var sourcesId = ""
    private var language = ""
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentArticleBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        setupSearchView()
    }

    private fun setupView() {
        sourcesId = ArticleFragmentArgs.fromBundle(arguments as Bundle).source
        language = ArticleFragmentArgs.fromBundle(arguments as Bundle).language

        articleAdapter = ArticleAdapter()
        with(binding.rvArticle) {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = articleAdapter.withLoadStateFooter(
                footer = LoadingStateAdapter {
                    articleAdapter.retry()
                }
            )
        }

        getData("")

        articleAdapter.addLoadStateListener { loadState ->
            when (loadState.refresh) {
                is LoadState.Loading -> showLoading(true)
                is LoadState.NotLoading -> showLoading(false)
                is LoadState.Error -> {
                    binding.tvMessage.visibility = View.VISIBLE
                    binding.tvMessage.text = "Unknown Error"
                }

            }
        }


        articleAdapter.setOnArticleItemClickCallback(object : OnArticleItemClickCallback {
            override fun onItemClicked(data: ArticleEntity) {
                nextScreen(data)
            }
        })

    }

    private fun getData(q: String) {
        articleViewModel.getListSources(sourcesId,language = language, q = q)
        articleViewModel.articles.flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
            .onEach { resource ->
                articleResponse(resource)
            }.launchIn(lifecycleScope)
    }

    private fun setupSearchView() {
        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            android.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                getData(p0 ?: "")
                return true
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                if (p0?.isEmpty() == true || p0 == null) {
                    getData("")
                }
                return true
            }
        })
    }

    private fun showLoading(isLoading: Boolean) {
        binding.progressBar.alpha = if (isLoading) 1.0f else 0.0f
        binding.tvMessage.alpha = if (isLoading) 0.0f else 1.0f
    }

    private fun articleResponse(resource: PagingData<ArticleEntity>?) {
        resource?.let {
            articleAdapter.submitData(lifecycle, it)
        }
    }


    private fun nextScreen(data: ArticleEntity) {
        val toDetailArticleFragment =
            ArticleFragmentDirections.actionArticleFragmentToDetailArticleFragment()
        toDetailArticleFragment.url = data.url
        view?.findNavController()?.navigate(toDetailArticleFragment)
    }

}