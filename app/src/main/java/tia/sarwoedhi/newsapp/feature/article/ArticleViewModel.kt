package tia.sarwoedhi.newsapp.feature.article

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import tia.sarwoedhi.newsapp.domain.entities.ArticleEntity
import tia.sarwoedhi.newsapp.domain.usecase.ArticleUseCase
import javax.inject.Inject


@HiltViewModel
class ArticleViewModel @Inject constructor(private val articleUseCase: ArticleUseCase) :
    ViewModel() {

    private val _articles = MutableStateFlow(PagingData.empty<ArticleEntity>())
    val articles: StateFlow<PagingData<ArticleEntity>> get() = _articles

    fun getListSources(sourcesId: String, language:String, q:String) {
        viewModelScope.launch {
            articleUseCase.invoke(sourcesId, lang = language, q = q).cachedIn(viewModelScope).collect { result ->
                _articles.value = result
            }
        }
    }

}