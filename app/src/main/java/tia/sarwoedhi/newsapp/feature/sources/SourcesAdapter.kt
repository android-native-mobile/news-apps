package tia.sarwoedhi.newsapp.feature.sources

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import tia.sarwoedhi.newsapp.R
import tia.sarwoedhi.newsapp.databinding.ItemSourcesBinding
import tia.sarwoedhi.newsapp.domain.entities.SourcesEntity

class SourcesAdapter : RecyclerView.Adapter<SourcesAdapter.ViewHolder>() {

    private lateinit var sourcesItemClickCallback: OnSourcesItemClickCallback

    private val sourcesList: MutableList<SourcesEntity> = mutableListOf()

    fun setOnSourcesItemClickCallback(onSourcesItemClickCallback: OnSourcesItemClickCallback) {
        this.sourcesItemClickCallback = onSourcesItemClickCallback
    }

    fun clearData() {
        sourcesList.clear()
        notifyDataSetChanged()
    }


    fun setData(items: List<SourcesEntity>) {
        sourcesList.clear()
        sourcesList.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = ItemSourcesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = sourcesList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindArticle(holder.itemView.context, sourcesList[position])
        holder.itemView.setOnClickListener {
            sourcesItemClickCallback.onItemClicked(sourcesList[position])
        }
    }

    inner class ViewHolder(private val binding: ItemSourcesBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindArticle(context: Context, entity: SourcesEntity) {
            with(binding) {
                if (entity.name.isNotBlank()) {
                    this.txtTitle.text = String.format(
                        context.resources.getString(R.string.list_contents),
                        entity.name
                    )
                }
                if (entity.url.isNotBlank()) {
                    this.txtUrl.text = String.format(
                        context.resources.getString(R.string.list_contents),
                        entity.url
                    )
                }
                if (entity.desc.isNotBlank()) {
                    this.txtOverview.text = String.format(
                        context.resources.getString(R.string.list_contents),
                        entity.desc
                    )
                }
            }
        }
    }
}

interface OnSourcesItemClickCallback {
    fun onItemClicked(data: SourcesEntity)
}