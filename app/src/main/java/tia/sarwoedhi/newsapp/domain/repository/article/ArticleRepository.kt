package tia.sarwoedhi.newsapp.domain.repository.article

import androidx.paging.PagingData
import kotlinx.coroutines.flow.Flow
import tia.sarwoedhi.newsapp.domain.entities.ArticleEntity

interface ArticleRepository {
    fun getListArticle(sources: String,language:String, q:String): Flow<PagingData<ArticleEntity>>
}