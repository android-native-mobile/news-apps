package tia.sarwoedhi.newsapp.domain.repository.category

import tia.sarwoedhi.newsapp.domain.entities.CategoryEntity
import tia.sarwoedhi.newsapp.domain.entities.SourcesEntity
import tia.sarwoedhi.newsapp.domain.utils.DomainWrapper

interface CategoryRepository {
    suspend fun getCategoryList(): DomainWrapper<List<CategoryEntity>>
    suspend fun getSourcesList(category : String, name:String): DomainWrapper<List<SourcesEntity>>
}