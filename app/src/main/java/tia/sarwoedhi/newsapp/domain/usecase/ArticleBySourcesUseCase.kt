package tia.sarwoedhi.newsapp.domain.usecase

import androidx.paging.PagingData
import kotlinx.coroutines.flow.Flow
import tia.sarwoedhi.newsapp.domain.entities.ArticleEntity
import tia.sarwoedhi.newsapp.domain.repository.article.ArticleRepository
import javax.inject.Inject

class ArticleBySourcesUseCase @Inject constructor(
    private val articleRepository: ArticleRepository
) {
    operator fun invoke(sourcesSelected: String, query:String, language:String): Flow<PagingData<ArticleEntity>>
    = articleRepository.getListArticle(sourcesSelected, q = query, language = language)

}