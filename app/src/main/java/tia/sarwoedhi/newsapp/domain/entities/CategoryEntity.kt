package tia.sarwoedhi.newsapp.domain.entities

data class CategoryEntity(
    val category: String = "",
)