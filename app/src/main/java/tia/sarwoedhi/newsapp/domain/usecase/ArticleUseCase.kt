package tia.sarwoedhi.newsapp.domain.usecase

import androidx.paging.PagingData
import kotlinx.coroutines.flow.Flow
import tia.sarwoedhi.newsapp.domain.entities.ArticleEntity
import tia.sarwoedhi.newsapp.domain.repository.article.ArticleRepository
import javax.inject.Inject

class ArticleUseCase @Inject constructor(private val repository: ArticleRepository) {

    operator fun invoke(sources: String, q: String, lang: String): Flow<PagingData<ArticleEntity>> {
        return repository.getListArticle(sources, language = lang, q = q)
    }

}