package tia.sarwoedhi.newsapp.domain.mapper

import tia.sarwoedhi.newsapp.core.data.remote.model.response.article.ArticleItem
import tia.sarwoedhi.newsapp.core.data.remote.model.response.category.Source
import tia.sarwoedhi.newsapp.domain.entities.ArticleEntity
import tia.sarwoedhi.newsapp.domain.entities.CategoryEntity
import tia.sarwoedhi.newsapp.domain.entities.SourcesEntity
import java.util.Locale

fun ArticleItem?.toArticleEntity(): ArticleEntity {
    return ArticleEntity(
        urlToImage = this?.urlToImage.orEmpty(),
        description = this?.description.orEmpty(),
        title = this?.title.orEmpty(),
        url = this?.url.orEmpty()
    )
}

fun Source?.toCategoryEntity(): CategoryEntity {
    return CategoryEntity(
        category = this?.category?.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.ROOT) else it.toString() }.orEmpty(),
     )
}


fun Source?.toSourcesEntity(): SourcesEntity {
    return SourcesEntity(
        id = this?.id.orEmpty(),
        desc = this?.description.orEmpty(),
        name = this?.name.orEmpty(),
        url = this?.url.orEmpty()
    )
}