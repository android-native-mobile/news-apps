package tia.sarwoedhi.newsapp.domain.usecase

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import tia.sarwoedhi.newsapp.domain.entities.SourcesEntity
import tia.sarwoedhi.newsapp.domain.repository.category.CategoryRepository
import tia.sarwoedhi.newsapp.domain.utils.DomainWrapper
import tia.sarwoedhi.newsapp.utils.UiState
import javax.inject.Inject

class SourcesUseCase @Inject constructor(private val repository: CategoryRepository) {

    operator fun invoke(category: String, name:String ): Flow<UiState<List<SourcesEntity>>> = flow {
        when(val result = repository.getSourcesList(category, name)) {
            is DomainWrapper.Success -> {
                emit(UiState.Success(result.data))
            }

            is DomainWrapper.Error -> {
                emit(UiState.Error(result.statusResponse ?: "Err"))
            }
        }
    }

}