package tia.sarwoedhi.newsapp.domain.entities

data class ArticleEntity(
    val urlToImage: String = "",
    val description: String = "",
    val title: String = "",
    val url: String = "",
    val id:String = ""
)