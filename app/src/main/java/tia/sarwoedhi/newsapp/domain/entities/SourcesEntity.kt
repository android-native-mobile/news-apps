package tia.sarwoedhi.newsapp.domain.entities

data class SourcesEntity(
    val id: String = "",
    val name: String = "",
    val desc: String = "",
    val url: String = "",
)